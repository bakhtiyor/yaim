package net.esector.jira.im.transport;

import java.io.IOException;
import java.util.Map;

import ymsg.network.AccountLockedException;
import ymsg.network.LoginRefusedException;
import ymsg.network.Session;
import ymsg.network.StatusConstants;
import ymsg.network.event.SessionAdapter;
import ymsg.network.event.SessionEvent;

public class YahooMessengerTransport implements IMTransport {

	private Session yahooSession;

	private final String YAHOO_ID = "Yahoo ID";

	private final String YAHOO_PASSWORD = "YAHOO Password";

	private String yahooId;

	private String yahooPassword;

	private TransportListener chatListener;

	public void connect() {
		if (!isConnected() && yahooId != null && yahooPassword != null && !yahooId.isEmpty()) {
			yahooSession = new Session();
			try {
				yahooSession.login(yahooId, yahooPassword);
			} catch (AccountLockedException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (LoginRefusedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public String[] getAcceptedParams() {
		return new String[] { YAHOO_ID, YAHOO_PASSWORD };
	}

	public boolean isConnected() {
		boolean result = false;
		if (yahooSession != null) {
			result = yahooSession.getSessionStatus() == StatusConstants.MESSAGING;
		}
		return result;
	}

	public void sendMessage(String to, String msg) {
		if (isConnected()) {
			try {
				yahooSession.sendMessage(to, msg);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public void setParams(Map params) {
		if (params.containsKey(YAHOO_ID)) {
			yahooId = (String) params.get(YAHOO_ID);
		}
		if (params.containsKey(YAHOO_PASSWORD)) {
			yahooPassword = (String) params.get(YAHOO_PASSWORD);
		}
	}

	public void setTransportListener(TransportListener listener) {
		chatListener = listener;
		yahooSession.addSessionListener(new SessionAdapter() {
			public void messageReceived(SessionEvent ev) {
				try {
					yahooSession.sendMessage(ev.getFrom(), chatListener
							.processMessage(ev.getFrom(), ev.getMessage()));
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}
	
	public IMStatus getContactStatus(String contact) {
		IMStatus result = IMStatus.OFFLINE;
		if (isConnected()) {
			long status = yahooSession.getUser(contact).getStatus();
			if (status == StatusConstants.STATUS_AVAILABLE
					|| status == StatusConstants.STATUS_TYPING) {
				result = IMStatus.ONLINE;
			} else if (status == StatusConstants.STATUS_BUSY
					|| status == StatusConstants.STATUS_ONPHONE) {
				result = IMStatus.BUSY;
			} else if (status == StatusConstants.STATUS_IDLE 
					|| status == StatusConstants.STATUS_NOTATDESK) {
				result = IMStatus.AWAY;
			} else if (status == StatusConstants.STATUS_BRB
					|| status == StatusConstants.STATUS_NOTINOFFICE
					|| status == StatusConstants.STATUS_ONVACATION
					|| status == StatusConstants.STATUS_OUTTOLUNCH
					|| status == StatusConstants.STATUS_STEPPEDOUT) {
				result = IMStatus.AWAY_LONG;
			}
		}
		return result;
	}

}
