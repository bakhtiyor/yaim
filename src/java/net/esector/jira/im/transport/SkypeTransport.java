package net.esector.jira.im.transport;

import java.util.Map;
import com.skype.ChatMessage;
import com.skype.ChatMessageAdapter;
import com.skype.Skype;
import com.skype.SkypeException;
import com.skype.User.Status;

public class SkypeTransport implements IMTransport {

	private TransportListener chatListener;
	
	public void connect() {

	}

	public String[] getAcceptedParams() {
		return new String[0];
	}

	public boolean isConnected() {
		boolean result = false;
		try {
			result = Skype.isRunning();
		} catch (SkypeException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void sendMessage(String to, String msg) {
		try {
			Skype.chat(to).send(msg);
		} catch (SkypeException e) {
			e.printStackTrace();
		}
	}

	public void setParams(Map params) {
		// TODO Auto-generated method stub

	}

	public void setTransportListener(TransportListener listener) {
		chatListener = listener;
		Skype.setDeamon(false); // to prevent exiting from this program
        try {
			Skype.addChatMessageListener(new ChatMessageAdapter() {
			    public void chatMessageReceived(ChatMessage received) throws SkypeException {
			        if (received.getType().equals(ChatMessage.Type.SAID)) {
			            received.getSender().send(chatListener.processMessage(
			            		received.getSender().getId(),
			            		received.getContent()
			            ));
			        }
			    }
			});
		} catch (SkypeException e) {
			e.printStackTrace();
		}
	}
	
	public IMStatus getContactStatus(String contact) {
		IMStatus result = IMStatus.OFFLINE;
		if (isConnected()) {
			try {
				Status status = Skype.getUser(contact).getOnlineStatus();
				if (status == Status.ONLINE || status == Status.SKYPEME) {
					result = IMStatus.ONLINE;
				} else if (status == Status.DND || status == Status.SKYPEOUT) {
					result = IMStatus.BUSY;
				} else if (status == Status.NA) {
					result = IMStatus.AWAY;
				} else {
					result = IMStatus.OFFLINE;
				}
			} catch (SkypeException e) {
				result = IMStatus.OFFLINE;
			}
		}
		return result;
	}

}
