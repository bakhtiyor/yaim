package net.esector.jira.im.transport;

import java.util.Map;

public interface IMTransport {
	public enum IMStatus {OFFLINE, ONLINE, BUSY, AWAY, AWAY_LONG };
	
	public String[] getAcceptedParams();

	public void setParams(Map params);
	
	public void connect();
	
	public boolean isConnected();
	
	public void sendMessage(String to, String msg);
	
	public void setTransportListener(TransportListener listener);
	
	public IMStatus getContactStatus(String contact);
}
