package net.esector.jira.im.transport;

import java.util.Map;

import net.sf.jml.Email;
import net.sf.jml.MsnContact;
import net.sf.jml.MsnContactList;
import net.sf.jml.MsnMessenger;
import net.sf.jml.MsnSwitchboard;
import net.sf.jml.MsnUserStatus;
import net.sf.jml.event.MsnMessageAdapter;
import net.sf.jml.impl.MsnMessengerFactory;
import net.sf.jml.message.MsnControlMessage;
import net.sf.jml.message.MsnDatacastMessage;
import net.sf.jml.message.MsnInstantMessage;

public class MsnMessangerTransport implements IMTransport {
	private MsnMessenger messenger;

	private final String MSN_EMAIL = "MSN Email";

	private final String MSN_PASSWORD = "MSN Password";

	private String msnEmail;

	private String msnPassword;

	private TransportListener chatListener;

	public void connect() {
		if (messenger == null && !msnEmail.isEmpty() && !msnPassword.isEmpty()) {
			messenger = MsnMessengerFactory.createMsnMessenger(msnEmail,
					msnPassword);
			messenger.login();
		}

	}

	public String[] getAcceptedParams() {
		return new String[] { MSN_EMAIL, MSN_PASSWORD };
	}

	public boolean isConnected() {
		boolean result = false;
		if (messenger != null) {
			result = true;
		}
		return result;
	}

	public void sendMessage(String to, String msg) {
		if (isConnected()) {
			MsnContactList cl = messenger.getContactList();
			Email email = Email.parseStr(to);
			MsnContact contact = cl.getContactByEmail(email);
			if (contact != null) {
				messenger.sendText(contact.getEmail(), msg);
			}
		}

	}

	public void setParams(Map params) {

		if (params.containsKey(MSN_EMAIL)) {
			msnEmail = (String) params.get(MSN_EMAIL);
		}
		if (params.containsKey(MSN_PASSWORD)) {
			msnPassword = (String) params.get(MSN_PASSWORD);
		}
		connect();
	}

	public void setTransportListener(TransportListener listener) {
		chatListener = listener;
		messenger.addMessageListener(new MsnMessageAdapter() {

			public void instantMessageReceived(MsnSwitchboard switchboard,
					MsnInstantMessage message, MsnContact contact) {
				// text message received
				switchboard.sendText(chatListener.processMessage(contact
						.getEmail().toString(), message.getContent()));
			}

			public void controlMessageReceived(MsnSwitchboard switchboard,
					MsnControlMessage message, MsnContact contact) {
			}

			public void datacastMessageReceived(MsnSwitchboard switchboard,
					MsnDatacastMessage message, MsnContact contact) {
			}

		});
	}
	
	public IMStatus getContactStatus(String contact) {
		IMStatus result = IMStatus.OFFLINE;
		if (isConnected()) {
			MsnContactList cl = messenger.getContactList();
			Email email = Email.parseStr(contact);
			if (email != null) {
				MsnUserStatus status = cl.getContactByEmail(email).getStatus();
				if (status == MsnUserStatus.ONLINE) {
					result = IMStatus.ONLINE;
				} else if (status == MsnUserStatus.BUSY
						|| status == MsnUserStatus.ON_THE_PHONE) {
					result = IMStatus.BUSY;
				} else if (status == MsnUserStatus.AWAY
						|| status == MsnUserStatus.IDLE) {
					result = IMStatus.AWAY;
				} else if (status == MsnUserStatus.OUT_TO_LUNCH
						|| status == MsnUserStatus.BE_RIGHT_BACK) {
					result = IMStatus.AWAY_LONG;
				} else {
					result = IMStatus.OFFLINE;
				}
			}
		}
		return result;
	}

}
