package net.esector.jira.im.transport;

public interface TransportListener {
	public String processMessage(String to, String msg);
}
