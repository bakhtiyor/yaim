/**
 * 
 */
package net.esector.jira.im.transport;

/**
 * @author �������
 * 
 */

import java.util.Map;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.packet.Presence.Type;

public class JabberTransport implements IMTransport {
	private XMPPConnection xmppConnection;

	private final String XMPP_SERVER = "XMPP Server";

	private final String XMPP_LOGIN = "XMPP Login";

	private final String XMPP_PASSWORD = "XMPP Password";

	private final String XMPP_PORT = "XMPP Port (Default 5222)";

	private final int DEFAULT_PORT = 5222;

	private String xmppServer;

	private String xmppLogin;

	private String xmppPassword;

	private int xmppPort;

	private TransportListener chatListener;

	private MessageListener aMsgListener;

	public JabberTransport() {
		xmppPort = DEFAULT_PORT;
	}

	public String[] getAcceptedParams() {
		return new String[] { XMPP_SERVER, XMPP_LOGIN, XMPP_PASSWORD,
				XMPP_PORT};
	}

	public void setParams(Map params) {
		if (params.containsKey(XMPP_SERVER)) {
			xmppServer = (String) params.get(XMPP_SERVER);
		}
		if (params.containsKey(XMPP_LOGIN)) {
			xmppLogin = (String) params.get(XMPP_LOGIN);
		}
		if (params.containsKey(XMPP_PASSWORD)) {
			xmppPassword = (String) params.get(XMPP_PASSWORD);
		}
		if (params.containsKey(XMPP_PORT)) {
			try {
				xmppPort = Integer.parseInt((String) params.get(XMPP_PORT));
			} catch (NumberFormatException e) {
				xmppPort = DEFAULT_PORT;
			}
		}
	}

	public void connect() {
		if (isConnected())
			return;
		if ((xmppServer != null && !xmppServer.isEmpty())
				&& (xmppLogin != null && !xmppLogin.isEmpty())
				&& (xmppPassword != null)) {
			ConnectionConfiguration xmppConfiguration = new ConnectionConfiguration(
					xmppServer, xmppPort);
			xmppConfiguration.setReconnectionAllowed(true);
			xmppConfiguration.setSelfSignedCertificateEnabled(true);
			xmppConnection = new XMPPConnection(xmppConfiguration);
			try {
				xmppConnection.connect();
				xmppConnection.login(xmppLogin, xmppPassword);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isConnected() {
		Boolean result = false;
		if (xmppConnection != null) {
			result = xmppConnection.isConnected();
		}
		return result;
	}

	public synchronized void sendMessage(String toJID, String msg) {
		if (isConnected()) {
			try {
				Roster roster = xmppConnection.getRoster();
				if (!roster.contains(toJID)) {
					roster.createEntry(toJID, toJID,
							new String[] { "NotificationRecipients" });
				}
				Chat chat = xmppConnection.getChatManager().createChat(toJID,
						null);
				chat.sendMessage(msg);
			} catch (XMPPException e) {
				e.printStackTrace();
			}
		}
	}

	public void setTransportListener(TransportListener listener) {
		chatListener = listener;
		if (isConnected()) {
			aMsgListener = new MessageListener() {
				public void processMessage(Chat chat, Message msg) {
					try {
						chat.sendMessage(chatListener.processMessage(
							cleanJID(chat.getParticipant()), msg.getBody()));
					} catch (XMPPException e) {
						e.printStackTrace();
					}
				}
			};
			xmppConnection.getChatManager().addChatListener(
					new ChatManagerListener() {
						public void chatCreated(Chat chat, boolean local) {
							if (!local) {
								chat.addMessageListener(aMsgListener);
							}
						}
					});
		}
	}

	public String cleanJID(String jid) {
		String result = jid;
		if (jid.contains("/")) {
			result = jid.substring(0, jid.indexOf("/"));
		}
		return result;
	}

	public IMStatus getContactStatus(String contact) {
		IMStatus result = IMStatus.OFFLINE;
		if (isConnected()) {
			Presence presence = xmppConnection.getRoster().getPresence(contact);

			if (presence.getType() == Type.available) {
				result = IMStatus.ONLINE;

				Mode status = presence.getMode();
				if (status == Mode.available || status == Mode.chat) {
					result = IMStatus.ONLINE;
				} else if (status == Mode.dnd) {
					result = IMStatus.BUSY;
				} else if (status == Mode.away) {
					result = IMStatus.AWAY;
				} else if (status == Mode.xa) {
					result = IMStatus.AWAY_LONG;
				}
			}
		}
		return result;
	}

}
