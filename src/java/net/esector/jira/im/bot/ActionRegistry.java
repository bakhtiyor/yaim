package net.esector.jira.im.bot;

import net.esector.jira.im.bot.action.BotAction;
import net.esector.jira.im.bot.action.CommentAction;
import net.esector.jira.im.bot.action.HelpAction;
import net.esector.jira.im.bot.action.InvalidAction;
import net.esector.jira.im.bot.action.StartProgressAction;
import net.esector.jira.im.bot.action.StopProgressAction;
import net.esector.jira.im.bot.action.UnauthorizedAction;
import net.esector.jira.im.bot.action.ViewAction;

public class ActionRegistry {

    public static final String COMMAND_VIEW = "view";

    public static final String COMMAND_COMMENT = "comment";

    public static final String COMMAND_START = "start";

    public static final String COMMAND_STOP = "stop";

    public static final String COMMAND_HELP = "yaim-help";
    
    public static final String INTERNAL_COMMAND_UNAUTHORIZED = "unauthorized";
    
    public static final String INTERNAL_COMMAND_INVALID = "invalid";
    
    public static BotAction createActionInternal(String command) {
        if (INTERNAL_COMMAND_UNAUTHORIZED.equals(command)) {
            return new UnauthorizedAction();
        } else if(INTERNAL_COMMAND_INVALID.equals(command)){
            return new InvalidAction();
        }
        return null;
    }

    public static BotAction createAction(String command) {
        if (COMMAND_VIEW.equals(command)) {
            return new ViewAction();
        } else if (COMMAND_COMMENT.equals(command)) {
            return new CommentAction();
        } else if (COMMAND_START.equals(command)) {
            return new StartProgressAction();
        } else if (COMMAND_STOP.equals(command)) {
            return new StopProgressAction();
        } else if (COMMAND_HELP.equals(command)) {
            return new HelpAction();
        }
        return null;
    }
}
