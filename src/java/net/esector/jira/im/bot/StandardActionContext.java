package net.esector.jira.im.bot;

import com.opensymphony.user.User;

public class StandardActionContext implements ActionContext {
    
    private final IMBot bot;

    private final User remoteUser;

    private final String message;

    private String command;

    private String queryString;
    
    public StandardActionContext(IMBot bot, User remoteUser, String message) {
        this.bot = bot;
        this.remoteUser = remoteUser;
        this.message = message;
    }
    
    public IMBot getBot() {
        return this.bot;
    }
    
    public User getRemoteUser() {
        return remoteUser;
    }
    
    public String getBaseUrl() {
        return bot.getBaseUrl();
    }

    public String getMessage() {
        return this.message;
    }

    public String getCommand() {
        return this.command;
    }
    
    public void setCommand(String command) {
        this.command = command;
    }

    public String getQueryString() {
        return this.queryString;
    }
    
    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
    
    public String getCommandPrefix() {
        return IMBot.COMMAND_PREFIX;
    }
}
