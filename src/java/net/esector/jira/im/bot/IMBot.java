package net.esector.jira.im.bot;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.esector.jira.im.bot.action.BotAction;
import net.esector.jira.im.transport.TransportListener;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.PermissionManager;
import com.opensymphony.user.User;
import com.opensymphony.user.UserManager;

public class IMBot implements TransportListener {

    public static final String COMMAND_PREFIX = "/";

    private static final Pattern COMMAND_REGEX = Pattern.compile("^"
            + Pattern.quote(COMMAND_PREFIX) + "([\\w\\-]+)(?:\\s+(.*))?$",
            Pattern.DOTALL);

    private final IssueManager issueManager;

    private final PermissionManager permissionManager;

    private final String imIDProperty;

    private final String baseUrl;

    public IMBot(IssueManager issueManager,
            PermissionManager permissionManager, String imIDProperty) {

        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.imIDProperty = imIDProperty;

        this.baseUrl = ComponentManager.getInstance()
                .getApplicationProperties().getString(APKeys.JIRA_BASEURL);
    }

    public String processMessage(String from, String message) {
        User remoteUser = getUserByProperty(imIDProperty, from);
        ActionContext ctx = createActionContext(remoteUser, message);
        BotAction action = createActionFrom(ctx);
        return action.proccess();
    }

    public IssueManager getIssueManager() {
        return this.issueManager;
    }

    public PermissionManager getPermissionManager() {
        return this.permissionManager;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    private ActionContext createActionContext(User remoteUser, String message) {
        ActionContext result = new StandardActionContext(this, remoteUser,
                message);

        String[] args = parseMessage(message);
        if (args != null) {
            result.setCommand(args[0]);
            result.setQueryString(args[1]);
        }

        return result;
    }

    private BotAction createActionFrom(ActionContext ctx) {
        BotAction result = null;
        if (ctx.getRemoteUser() != null) {
            result = ActionRegistry.createAction(ctx.getCommand());
            if (result == null) {
                result = ActionRegistry
                        .createActionInternal(ActionRegistry.INTERNAL_COMMAND_INVALID);
            }
        } else {
            result = ActionRegistry
                    .createActionInternal(ActionRegistry.INTERNAL_COMMAND_UNAUTHORIZED);
        }
        result.setActionContext(ctx);
        return result;
    }

    private User getUserByProperty(String property, String value) {
        User result = null;

        // TODO find better(documented) way to lookup user by property if
        // possible. Or introduce some sort of caching, WeakHashMap will do
        List<User> users = UserManager.getInstance().getUsers();
        for (User u : users) {
            if (u.getPropertySet().exists(property)
                    && u.getPropertySet().getString(property).equals(value)) {
                result = u;
                break;
            }
        }
        return result;
    }

    private static String[] parseMessage(String message) {
        String[] result = null;
        Matcher m = COMMAND_REGEX.matcher(message);
        if (m.find()) {
            result = new String[] { m.group(1), m.group(2) };
        }
        return result;
    }

}
