package net.esector.jira.im.bot.action;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.jira.issue.Issue;

public abstract class AbstractIssueAction extends AbstractVelocityAction {

    private static final Pattern COMMAND_REGEX = Pattern
            .compile("^([A-Z]+-\\d+)(?:\\s+(.*))?$", Pattern.DOTALL);

    protected Issue issue;

    @Override
    protected String proccessModel(Map<String, Object> model) {

        Matcher m = COMMAND_REGEX.matcher(ctx.getQueryString());

        if (m.find()) {

            String key = m.group(1);
            issue = ctx.getBot().getIssueManager().getIssueObject(key);

            if (issue != null) {

                if (hasPermissions()) {
                    model.put("issue", issue);
                    return proccessIssue(m.group(2), model);
                } else {
                    return unauthorizedActionResult();
                }

            } else {
                return "Issue '" + key + "' not found.";
            }
        }
        return getHelp();
    }

    protected abstract String proccessIssue(String args,
            Map<String, Object> model);

    protected boolean hasPermissions() {
        for (int permission : getRequiredPermissions())
            if (!ctx.getBot().getPermissionManager().hasPermission(permission,
                    issue, ctx.getRemoteUser()))
                return false;

        return true;
    }

    protected abstract int[] getRequiredPermissions();

}
