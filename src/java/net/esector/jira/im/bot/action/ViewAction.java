package net.esector.jira.im.bot.action;

import java.util.Map;

import net.esector.jira.im.bot.ActionRegistry;

import com.atlassian.jira.security.Permissions;

public class ViewAction extends AbstractIssueAction {

    @Override
    protected int[] getRequiredPermissions() {
        return new int[] { Permissions.BROWSE };
    }

    @Override
    protected String proccessIssue(String args, Map<String, Object> model) {

        return getView("view.vm", model);
    }

    public String getHelp() {
        String command = ctx.getCommandPrefix() + ActionRegistry.COMMAND_VIEW;
        return "\n"
                + "- - - - - - - - - - - - - - - - - -\n"
                + "Usage: " + command + " <Issue Key>\n"
                + "Prints general information on issue with specified key\n";
    }

}
