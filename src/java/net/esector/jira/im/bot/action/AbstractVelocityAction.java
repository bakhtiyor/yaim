package net.esector.jira.im.bot.action;

import java.util.HashMap;
import java.util.Map;

import net.esector.jira.im.bot.ActionContext;
import net.esector.jira.im.bot.ActionRegistry;

import org.apache.velocity.exception.VelocityException;

import com.atlassian.jira.ComponentManager;

public abstract class AbstractVelocityAction implements BotAction {

    protected ActionContext ctx;

    protected static final String TEMPLATE_PATH = "templates/ya-im/actions/";

    public String proccess() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("base_url", ctx.getBaseUrl());
        
        return proccessModel(model);
    }

    protected abstract String proccessModel(Map<String, Object> model);

    public void setActionContext(ActionContext ctx) {
        this.ctx = ctx;
    }

    protected String getView(String tpl, Map<String, Object> model) {
        try {
            return ComponentManager.getInstance().getVelocityManager().getBody(
                    "", TEMPLATE_PATH + tpl, model);
        } catch (VelocityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null; // TODO return ErrorAction.proccess();
    }

    protected String unauthorizedActionResult() {
        BotAction action = ActionRegistry
                .createAction(ActionRegistry.INTERNAL_COMMAND_UNAUTHORIZED);
        action.setActionContext(ctx);
        return action.proccess();
    }

}
