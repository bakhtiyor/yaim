package net.esector.jira.im.bot.action;

import java.util.Map;

import net.esector.jira.im.bot.ActionRegistry;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;

public class StartProgressAction extends AbstractIssueAction {

    @Override
    protected int[] getRequiredPermissions() {
        return new int[] { Permissions.WORK_ISSUE };
    }

    @Override
    protected String proccessIssue(String args, Map<String, Object> model) {
        
        String username = ctx.getRemoteUser().getName();

        MutableIssue mIssue = (MutableIssue) issue;
        WorkflowTransitionUtil workflowTransitionUtil = (WorkflowTransitionUtil) JiraUtils
                .loadComponent(WorkflowTransitionUtilImpl.class);

        workflowTransitionUtil.setIssue(mIssue);
        workflowTransitionUtil.setUsername(username);
        workflowTransitionUtil.setAction(4);
        workflowTransitionUtil.validate();
        workflowTransitionUtil.progress();

        return getView("start.vm", model);
    }

    public String getHelp() {
        String command = ctx.getCommandPrefix() + ActionRegistry.COMMAND_START;
        return "\n"
                + "- - - - - - - - - - - - - - - - - -\n"
                + "Usage: " + command + " <Issue Key>\n"
                + "Starts progress on issue with specified key\n";
    }

}
