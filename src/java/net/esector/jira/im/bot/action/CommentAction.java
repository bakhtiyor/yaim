package net.esector.jira.im.bot.action;

import java.util.Map;

import net.esector.jira.im.bot.ActionRegistry;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.security.Permissions;

public class CommentAction extends AbstractIssueAction {

    @Override
    protected int[] getRequiredPermissions() {
        return new int[] { Permissions.COMMENT_ISSUE };
    }

    @Override
    protected String proccessIssue(String args, Map<String, Object> model) {

        if (args != null && !args.isEmpty()) {

            CommentManager cm = ComponentManager.getInstance()
                    .getCommentManager();
            Comment comment = cm.create(issue, ctx.getRemoteUser().getName(), args, true);
            model.put("comment", comment);
            return getView("comment.vm", model);
        }
        
        return "Comment text required.";
    }

    public String getHelp() {
        String command = ctx.getCommandPrefix() + ActionRegistry.COMMAND_COMMENT;
        return "\n"
                + "- - - - - - - - - - - - - - - - - -\n"
                + "Usage: " + command + " <Issue Key> <Comment Text>\n"
                + "Comments issue with specified key\n";
    }

}
