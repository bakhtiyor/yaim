package net.esector.jira.im.bot.action;

import net.esector.jira.im.bot.ActionContext;
import net.esector.jira.im.bot.ActionRegistry;

public class InvalidAction implements BotAction {

    private ActionContext ctx;

    public String getHelp() {
        throw new UnsupportedOperationException();
    }

    public String proccess() {
        String msg = ctx.getMessage();
        if (msg != null && !msg.trim().equals("")) {
            String result = "";

            if (ctx.getCommand() != null) {

                result = "No such command `" + ctx.getCommand() + "`.";

            } else {

                result = "Message not understood.";

            }

            return result + " Type `" + ctx.getCommandPrefix()
                    + ActionRegistry.COMMAND_HELP
                    + "` to get the list of available commands.";
        }
        return null;
    }

    public void setActionContext(ActionContext ctx) {
        this.ctx = ctx;
    }

}
