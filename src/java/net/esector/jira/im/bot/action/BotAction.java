package net.esector.jira.im.bot.action;

import net.esector.jira.im.bot.ActionContext;

public interface BotAction {

    public void setActionContext(ActionContext ctx);

    public String proccess();
    
    public String getHelp();
}