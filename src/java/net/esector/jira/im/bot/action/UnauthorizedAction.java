package net.esector.jira.im.bot.action;

import net.esector.jira.im.bot.ActionContext;

public class UnauthorizedAction implements BotAction {

    public String getHelp() {
        throw new UnsupportedOperationException();
    }

    public String proccess() {
        return "Forbidden.";
    }

    public void setActionContext(ActionContext ctx) {

    }

}
