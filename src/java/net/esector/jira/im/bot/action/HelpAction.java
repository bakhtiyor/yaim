package net.esector.jira.im.bot.action;

import net.esector.jira.im.bot.ActionContext;
import net.esector.jira.im.bot.ActionRegistry;

public class HelpAction implements BotAction {

    private ActionContext ctx;

    public String proccess() {
        String command = ctx.getQueryString();
        if(command != null && !command.trim().equals("")) {
            
            BotAction action = ActionRegistry.createAction(command);
            
            if(action != null) {
                
                action.setActionContext(ctx);
                return action.getHelp();
            } else {
                
                return "Command `" + command + "` not found. Type `"
                        + ctx.getCommandPrefix() + ActionRegistry.COMMAND_HELP
                        + "` to get the list of available commands.";
            }
        }
        return getHelp();
    }

    public void setActionContext(ActionContext ctx) {
        this.ctx = ctx;
    }

    public String getHelp() {
        
        String help = ctx.getCommandPrefix() + ActionRegistry.COMMAND_HELP;
        
        return "\n"
            + "- - - - - - - - - - - - - - - - - -\n"
            + "Usage: " + help + " <command name>\n"
            + "Prints help on specified command\n"
            + "\n"
            + " Available commands are:\n"
            + " - " + ActionRegistry.COMMAND_VIEW + "\n"
            + " - " + ActionRegistry.COMMAND_COMMENT + "\n"
            + " - " + ActionRegistry.COMMAND_START + "\n"
            + " - " + ActionRegistry.COMMAND_STOP + "\n";
    }

}
