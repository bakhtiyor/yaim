package net.esector.jira.im.bot;

import com.opensymphony.user.User;

public interface ActionContext {
    
    public IMBot getBot();
    public User getRemoteUser();
    public String getBaseUrl();
    public String getMessage();
    
    public String getCommandPrefix();
    
    
    public void setQueryString(String queryString);
    public String getQueryString();
    
    public void setCommand(String command);
    public String getCommand();
    
}
