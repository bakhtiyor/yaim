package net.esector.jira.im.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.esector.jira.im.bot.IMBot;
import net.esector.jira.im.transport.IMTransport;
import net.esector.jira.im.transport.IMTransport.IMStatus;

import org.apache.log4j.Category;
import org.apache.velocity.exception.VelocityException;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventListener;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.velocity.VelocityManager;
import com.opensymphony.module.propertyset.PropertySet;

public abstract class AbstractIMNotifier extends AbstractIssueEventListener
        implements IssueEventListener {

    private static final Category log = Category
            .getInstance(AbstractIMNotifier.class);

    protected NotificationSchemeManager notificationSchemeManager;

    protected IssueManager issueManager;

    protected PermissionManager permissionManager;

    protected IMTransport transport;

    private final String tpl = "templates/ya-im/notify.vm";
    
    protected EnumSet<IMStatus> notifiableStatuses;

    protected AbstractIMNotifier(
            NotificationSchemeManager notificationSchemeManager,
            IssueManager issueManager, PermissionManager permissionManager,
            IMTransport transport) {
        this.notificationSchemeManager = notificationSchemeManager;
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.transport = transport;
        this.notifiableStatuses = EnumSet.of(IMStatus.ONLINE, IMStatus.AWAY);
    }

    public String[] getAcceptedParams() {
        List<String> params = new ArrayList<String>();
        params.addAll(Arrays.asList(transport.getAcceptedParams()));
        params.addAll(Arrays.asList(getAcceptedListenerParams()));
        return (String[]) params.toArray(new String[0]);
    }

    public void init(Map params) {
        transport.setParams(params);
        transport.connect();
        initListener(params);
    }

    protected abstract void initListener(Map params);

    protected abstract String[] getAcceptedListenerParams();

    protected abstract String getIMIDProperty();

    protected void sendNotification(IssueEvent event) {
        if (event.isSendMail() && (getIMIDProperty() != null)) {

            GenericValue projectGV = event.getIssue().getProject();
            GenericValue notificationScheme = notificationSchemeManager
                    .getNotificationSchemeForProject(projectGV);

            if (notificationScheme != null) {
                String imIDProperty = "jira.meta." + getIMIDProperty();
                String msg = getTemplatedMsg(event);
                HashSet<NotificationRecipient> recipients = getRecipients(
                        notificationScheme, event);

                for (NotificationRecipient recipient : recipients) {
                    PropertySet propertySet = recipient.getUser()
                            .getPropertySet();

                    if (propertySet.exists(imIDProperty)) {
                        String imID = propertySet.getString(imIDProperty);
                        if (isStatusNotifiable(transport.getContactStatus(imID))) {
                            transport.sendMessage(imID, msg);
                        }
                    }
                }
            }
        }
    }

    protected boolean isStatusNotifiable(IMStatus status) {
        return notifiableStatuses.contains(status);
    }

    protected void runBot() {
        String imIDProp = getIMIDProperty();
        if (transport.isConnected() && imIDProp != null
                && !"".equals(imIDProp.trim())) {
            transport.setTransportListener(new IMBot(issueManager,
                    permissionManager, "jira.meta." + imIDProp));
        }
    }

    @SuppressWarnings("unchecked")
    private HashSet<NotificationRecipient> getRecipients(
            GenericValue notificationScheme, IssueEvent event) {
        HashSet<NotificationRecipient> result = new HashSet<NotificationRecipient>();
        try {
            Collection<GenericValue> notificationSchemeEntities = notificationSchemeManager
                    .getEntities(notificationScheme, event.getEventTypeId());
            for (GenericValue schemeEntity : notificationSchemeEntities) {
                SchemeEntity notificationSchemeEntity = new SchemeEntity(
                        schemeEntity.getLong("id"), schemeEntity
                                .getString("type"), schemeEntity
                                .getString("parameter"), schemeEntity
                                .get("eventTypeId"), schemeEntity
                                .get("templateId"), schemeEntity
                                .getLong("scheme"));

                Collection<NotificationRecipient> intendedRecipients = notificationSchemeManager
                        .getRecipients(event, notificationSchemeEntity);

                if (intendedRecipients != null && !intendedRecipients.isEmpty()) {
                    for (NotificationRecipient notificationRecipient : intendedRecipients) {
                        if (!result.contains(notificationRecipient)) {
                            result.add(notificationRecipient);
                        }
                    }
                }
            }
        } catch (GenericEntityException e) {
            log.error(
                    "There was an error accessing the notifications for the scheme: "
                            + notificationScheme.getString("name") + ".", e);
        }
        return result;
    }

    public void workflowEvent(IssueEvent event) {
        sendNotification(event);
    }

    public boolean isInternal() {
        return false;
    }

    public boolean isUnique() {
        return true;
    }

    public abstract String getDescription();

    public String getTemplatedMsg(IssueEvent event) {
        Issue issue = event.getIssue();
        String result = "";
        VelocityManager vm = ComponentManager.getInstance()
                .getVelocityManager();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("issue", issue);
        params.put("event", event);
        String desc = ComponentManager.getInstance().getEventTypeManager()
                .getEventType(event.getEventTypeId()).getName();
        params.put("desc", desc);
        params.put("comment", event.getComment());
        String base_url = ComponentManager.getInstance()
                .getApplicationProperties().getString(APKeys.JIRA_BASEURL);
        params.put("base_url", base_url);
        try {
            result = vm.getBody("", tpl, params);
        } catch (VelocityException e) {
            e.printStackTrace();
        }
        return result;
    }
    
	protected <E extends Enum<E>> EnumSet<E> stringToEnumSet(Class<E> clazz, String str, String regex) {
		EnumSet<E> result = EnumSet.noneOf(clazz);
		result.clear();
		String[] values = str.split(regex);
		for(String value: values) {
			try {
				result.add(Enum.valueOf(clazz, value.trim()));
			} catch (IllegalArgumentException e) {
				continue;
			}
		}
		return result;
	}
}
