package net.esector.jira.im.listener;

import java.util.EnumSet;
import java.util.Map;

import net.esector.jira.im.transport.YahooMessengerTransport;
import net.esector.jira.im.transport.IMTransport.IMStatus;

import org.apache.log4j.Category;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.security.PermissionManager;

public class YimNotifierListener extends AbstractIMNotifier {

    private static final Category log = Category
            .getInstance(YimNotifierListener.class);

    private static final String IM_ID_PROPERTY = "Users Yahoo ID Property";

    private static final String ENABLE_BOT = "Enable YIM Bot (1/0)";

    private static final String NOTIFIABLE_STATUSES = "Notifitiable Statuses (Default: ONLINE,AWAY)";

    private String imIDProperty = null;

    private boolean enableBot = false;

    public YimNotifierListener() {
        this(null, null, null);
    }

    public YimNotifierListener(
            NotificationSchemeManager notificationSchemeManager,
            IssueManager issueManager, PermissionManager permissionManager) {
        super(notificationSchemeManager, issueManager, permissionManager,
                new YahooMessengerTransport());
    }

    @Override
    protected String[] getAcceptedListenerParams() {
        return new String[] { IM_ID_PROPERTY, ENABLE_BOT, NOTIFIABLE_STATUSES };
    }

    @Override
    public String getDescription() {
        return "Sends notifications about issue updates via Yahoo! Messenger.\n"
                + "Also may act like a bot and accept commands from remote users.\n"
                + "Notifiable statuses of remote users are fully configurable, available options are ONLINE, AWAY, AWAY_LONG, BUSY and OFFLINE.";
    }

    @Override
    protected String getIMIDProperty() {
        return imIDProperty;
    }

    @Override
    protected void initListener(Map params) {
        if (params.containsKey(IM_ID_PROPERTY)) {
            imIDProperty = (String) params.get(IM_ID_PROPERTY);
        }
        if (params.containsKey(ENABLE_BOT)) {
            enableBot = "1".equals((" " + params.get(ENABLE_BOT)).trim());
        }
        if (enableBot) {
            runBot();
        }
        if (params.containsKey(NOTIFIABLE_STATUSES)) {
            String statusesStr = (" " + params.get(NOTIFIABLE_STATUSES))
                    .trim().toUpperCase();
            EnumSet<IMStatus> statuses = stringToEnumSet(IMStatus.class,
                    statusesStr, "\\b");
            if (statuses.size() != 0) {
                notifiableStatuses = statuses;
            }
        }
    }

}
