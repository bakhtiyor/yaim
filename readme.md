# Yet Another Instant Messanger Plugin (YAIM) 
### (c) 2007 by Bakhtiyor Khodjayev and Sanjar Akhmedov

The JIRA extension sends issue event notifications and can work as a bot.
YAIM supports several IM protocols: XMPP/Jabber, MSN Messenger, Yahoo IM, 
Skype and can be easily extended to support another one.

## JIRA
Tested with JIRA 3.8.1

## CONTENTS
Included in this distribution:
* Full Source
* Dependant IM libraries

## Installation
Put jars from distribution package to classpath reachable place (if you use standalone JIRA place jars to JIRA_HOME/atlassian-jira/WEB-INF/lib).

Register new IM account for JIRA YAIM plugin (the account will send notifications and work as bot): e.g. for Yahoo IM go to http://yahoo.com/ and register new Yahoo ID. 

## REQUIREMENTS 
1. Sun Java JDK 1.5+ : http://java.sun.com/j2se/downloads/index.html
2. Apache Maven 1.0 :  http://maven.apache.org/start/download.html

Maven is not strictly required to build YAIM plugin


## DEPENDENCIES 
1. Smack v 3.0.2 - http://www.igniterealtime.org/projects/smack/index.jsp
2. Skype4Java v 1.0 - https://developer.skype.com/wiki/Java_API
3. YMSG Java API v 0.6.1 - http://jymsg9.sourceforge.net/
4. Java MSN Messenger Library v 1.0b1 - http://java-jml.sourceforge.net/
(All included into souce distribution)


